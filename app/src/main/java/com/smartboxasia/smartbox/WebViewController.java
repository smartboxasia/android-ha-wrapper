package com.smartboxasia.smartbox;

import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by zynick on 11/12/16.
 */

public class WebViewController extends WebViewClient{

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }

    /*
    // https://developer.android.com/guide/webapps/webview.html#AddingWebView
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (Uri.parse(url).getHost().equals("www.example.com")) {
            // This is my web site, so do not override; let my WebView load the page
            return false;
        }
        // Otherwise, the link is not for a page on my site, so launch another Activity that handles URLs
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
        return true;
    }
    */
}
