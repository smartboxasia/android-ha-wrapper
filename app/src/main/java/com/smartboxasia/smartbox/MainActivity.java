package com.smartboxasia.smartbox;

/**
 * http://stackoverflow.com/a/16349502/1150427
 * https://developer.android.com/guide/webapps/webview.html
 */

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;

/*
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
*/

public class MainActivity extends Activity {

    // Called when the activity is first created.
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        WebView myWebView = (WebView) findViewById(R.id.webview);

        // Enable Javascript
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        // Binding JavaScript code to Android code
         myWebView.addJavascriptInterface(new WebAppInterface(this), "android");

        // Handling Page Navigation
        myWebView.setWebViewClient(new WebViewController());

        myWebView.loadUrl("http://www.smartboxasia.com/showroom.html");
    }
}
